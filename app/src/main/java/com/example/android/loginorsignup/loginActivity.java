package com.example.android.loginorsignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class loginActivity extends AppCompatActivity {

    private EditText emailTxt  , passwordTxt;
    private TextView signUp , forget;
    private Button loginBtn;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailTxt = (EditText) findViewById(R.id.edt_email);
        passwordTxt = (EditText) findViewById(R.id.edt_password);
        signUp = (TextView) findViewById(R.id.signup);
        forget = (TextView) findViewById(R.id.forget);
        loginBtn = (Button)findViewById(R.id.btn_login);

        mAuth = FirebaseAuth.getInstance();
        setTitle("LOGIN!");
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if (valid()) {
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(loginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(), "Passed", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(loginActivity.this, welcomeActivity.class));
                                    } else {
                                        Toast.makeText(getApplicationContext(), "NOT FOUND", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(getApplicationContext() , signupActivity.class);
                startActivity(intent);
            }
        });
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext() , resetActivity.class));
            }
        });

    }
    public boolean valid(){
        boolean flag = true;
        String email = emailTxt.getText().toString();
        String password = passwordTxt.getText().toString();

        if(email.isEmpty()){
            emailTxt.setError("ENTER THE EMAIL");
            flag=false;
        }
        if (password.isEmpty()) {
            passwordTxt.setError("ENTER THE PASSWORD");
            flag=false;
        }

        return flag;
    }
}
