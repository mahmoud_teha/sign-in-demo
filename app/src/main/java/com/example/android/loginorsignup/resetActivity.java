package com.example.android.loginorsignup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class resetActivity extends AppCompatActivity {

    private EditText resetTxt ;
    private Button btn_reset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        setTitle("RESET!");

        resetTxt = (EditText) findViewById(R.id.edt_email_reset);
        btn_reset = (Button) findViewById(R.id.btn_reset) ;

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(valid()){
                FirebaseAuth.getInstance().sendPasswordResetEmail(resetTxt.getText().toString())
                        .addOnCompleteListener(resetActivity.this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(getApplicationContext() ,"you have been successfully Reset the password!" , Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getApplicationContext() , "INVALID EMAIL!" , Toast.LENGTH_SHORT).show();
                                    resetTxt.setError("INVALID EMAIL");
                                }
                            }
                        });
                }
            }
        });
    }
    public boolean valid(){
        boolean flag = true;
        String email = resetTxt.getText().toString();
        if(email.isEmpty()){
            resetTxt.setError("ENTER THE EMAIL");
            flag=false;
        }
        return flag;
    }

}
