package com.example.android.loginorsignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class signupActivity extends AppCompatActivity {

    private EditText emailTxt  , passwordTxt , rePasswordTxt , nameTxt;
    private TextView login;
    private Button signUpBtn;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        emailTxt = (EditText) findViewById(R.id.edt_email);
        passwordTxt = (EditText) findViewById(R.id.edt_password);
        rePasswordTxt = (EditText) findViewById(R.id.edt_reEnterPassword);
        nameTxt = (EditText) findViewById(R.id.edt_name);
        login = (TextView) findViewById(R.id.login);
        signUpBtn = (Button) findViewById(R.id.btn_create);
        mAuth = FirebaseAuth.getInstance();
        setTitle("SIGN UP!");
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if(valid()){
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(signupActivity.this , new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Passed", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(signupActivity.this  , welcomeActivity.class));
                            } else {
                                if (task.getException().getMessage().equals("The email address is already in use by another account.")) {
                                    Toast.makeText(getApplicationContext(), "THIS EMAIL IS ALREADY EXIST!", Toast.LENGTH_SHORT).show();

                                }
                                Toast.makeText(getApplicationContext(), "FAILED", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                }

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),loginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }


     public boolean valid(){
         boolean flag = true;
         String name = nameTxt.getText().toString();
         String email = emailTxt.getText().toString();
         String password = passwordTxt.getText().toString();
         String rePassword = rePasswordTxt.getText().toString();
         if(name.isEmpty() || name.length() <3){
             nameTxt.setError("AT LEAST 3 CHARACTER");
             flag=false;
         }
         if(email.isEmpty()){
             emailTxt.setError("ENTER AN EMAIL");
             flag=false;
         }
         if (password.isEmpty() || password.length() < 6 ) {
            passwordTxt.setError("AT LEAST 6 ALPHANUMERIC");
             flag=false;
         }
         if ( !(rePassword.equals(password))) {
            rePasswordTxt.setError("PASSWORD DON'T MATCH!");
             flag=false;
         }
         return flag;
    }
}
